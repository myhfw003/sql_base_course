SQL Server 2012 各版本介绍
目前，SQL Server 2012 分为主要版本和专业版。 

在选择版本的时候可以根据您具体的需要进行抉择，如果你需要一个免费的数据库管理系统，那么就选择 Compact 版本或 Express 版本；如果不确定你要使用什么版本的话也可以下载一个试用版，SQL Server 2012 试用版的免费使用时间为180天。

版本
主要版本
SQL Server 2012 的三个主要版如下：

企业版
考虑保费发售，企业版的目的是向大规模数据中心和数据仓库解决方案。数据管理和商业智能平台，提供企业级的可扩展性，高可用性和安全运行的关键任务应用。数据中心版的所有功能（这是包含在SQL Server的早期版本，但现已不用）在SQL Server 2012企业版。

标准版
标准版的目的是对那些通常出现在规模较小的组织或部门的部门数据库和有限的商业智能应用。

商业智能版 
针对向那些需要企业商业智能和自助服务功能，但不需要完整的在线事务处理（OLTP）性能和可扩展性到企业版。

SQL Server 2012 的三个专业版如下：

开发版（也被称为精简版）
免费，嵌入式软件开发人员可以使用ASP.NET构建网站和Windows桌面应用程序的数据库。

Web 版本
旨在支持面向Internet的工作负载，使企业能够快速部署网页，应用程序，网站和服务。

Express版本
SQL Server理想的学习和构建桌面和小型服务器应用程序的免费版本，限于每个数据库10GB存储空间，Express 版本中提供了以下功能：

SQL Server快捷与工具
包含连同管理SQL Server实例，包括SQL Server Express，LocalDB和SQL Azure的核心工具。

SQL Server 管理套件
不包含SQL Server数据库中，只有管理SQL Server实例的工具，其中包括的LocalDB，SQL 快速，SQL Azure等。如果你已经有了SQL Server数据库，只需要管理工具。

SQL Server Express LocalDB (MSI installer)
轻量级版本的SQL Server Express具有其全部功能及可编程，但是它运行在用户模式，具有快速，零配置安装。没有管理工具都包括在内。

SQL Server Express 及高级服务
包括数据库引擎，快速的工具，报表服务，全文检索，管理工具和SQL Server Express的所有组件。

SQL Server Express
这是核心Express数据库服务器。如果你需要接受远程连接或远程管理，并不需要使用工具或高级服务。
也可以下载它并安装用于演示和评估目的，有180天的试用期。这将使你尝试了完全成熟的企业版，在是否购买决定之前。